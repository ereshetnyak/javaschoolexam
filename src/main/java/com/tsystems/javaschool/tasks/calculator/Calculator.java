package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.text.DecimalFormat;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
    	try {
    		DecimalFormat decimalFormat = new DecimalFormat("##.####");
    		String rpn = toRPN(statement, constPriority);
            String[] elements = rpn.split(" ");
            Stack<Double> result = new Stack<Double>();
            
            for (int i = 0; i < elements.length; i++){
            	String el = elements[i];
            	if (!constPriority.keySet().contains(el)) {
                		result.push(new Double(el));
                } 
            	else {           	
                	Double operand2 = result.pop();
                	Double operand1 = result.empty() ? 0 : result.pop();
                	switch (el) {
                		case "*" : 	result.push(operand1 * operand2);
                		break;
                		case "/" : 	result.push(operand1 / operand2);
                		break;
                		case "+" : 	result.push(operand1 + operand2);
                		break;
                		case "-" : 	result.push(operand1 - operand2);
                		break;
                		case "^" : 	result.push(Math.pow(operand1, operand2));
                		break;
                		default: throw new RuntimeException();	
                	}                
                }
            }
            
            
            return decimalFormat.format(result.pop());
    	} catch (Exception e) {return null;} 
    }

    private static final Map<String, Integer> constPriority;	
	static {
		constPriority = new HashMap<String, Integer>();
		constPriority.put("^", 1);
		constPriority.put("*", 2);
		constPriority.put("/", 2);
		constPriority.put("-", 3);
		constPriority.put("+", 3);
    }
	
	private static String toRPN(String expression, Map<String, Integer> operations) {		
		List<String> rpnList = new ArrayList<String>();
		Stack<String> stackOp = new Stack<String>();
		Set<String> ops = new HashSet<String>(operations.keySet());
		String leftBracket = "(";
		String rightBracket = ")";
		ops.add(leftBracket);
		ops.add(rightBracket);
		
		expression = expression.replace(" ", "");
		
		int index = 0;
		boolean doIt = true;
		while (doIt) {
            int expLenght = expression.length();
            String nextOp = "";
   
            for (String op : ops) {
                int i = expression.indexOf(op, index);
                if (i >= 0 && i < expLenght) {
                    nextOp = op;
                    expLenght = i;
                }
            }            
            if (expLenght == expression.length()) {
                doIt = false;
            } 
            else {
                if (index != expLenght) {
                	rpnList.add(expression.substring(index, expLenght));
                }
                if (nextOp.equals(leftBracket)) {
                    stackOp.push(nextOp);
                }
                else if (nextOp.equals(rightBracket)) {
                    while (!stackOp.peek().equals(leftBracket)) {
                    	rpnList.add(stackOp.pop());
                        if (stackOp.empty()) {
                        	throw new RuntimeException();
                        }
                    }
                    stackOp.pop();
                } 
                else {
                    while (!stackOp.empty() && !stackOp.peek().equals(leftBracket) && (operations.get(nextOp)
                    			>= operations.get(stackOp.peek()))) {
                    	rpnList.add(stackOp.pop());
                    }
                    stackOp.push(nextOp);
                }
                index = expLenght + nextOp.length();
            }
        }
		
        if (index != expression.length()) {
            rpnList.add(expression.substring(index));
        }
        while (!stackOp.empty()) {
            rpnList.add(stackOp.pop());
        }
        StringBuffer rpn = new StringBuffer();
        if (!rpnList.isEmpty()) {
            rpn.append(rpnList.remove(0));
        }
        while (!rpnList.isEmpty()) {
            rpn.append(" ").append(rpnList.remove(0));
        }
        
        
        return rpn.toString();
	}
	
}
