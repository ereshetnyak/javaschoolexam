package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.lang.IllegalArgumentException;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
    	
    	if (x == null || y == null) {
			throw new IllegalArgumentException();
		}
    	if (x.isEmpty() && y.isEmpty()) {
			return true;
		}
    	if (y.isEmpty() ) {
			return false;
		}
    	
    	boolean match = true;
		int marker = 0;
		
		for (int xi = 0; xi < x.size(); xi++) {
			if (match != true) break;
			for (int yi = marker; yi < y.size(); yi++) {
				if (x.get(xi).equals(y.get(yi))) {
					match = true;
					marker = yi++;
					break;
				}
				else {
					match = false;
				}
			}
			
		
		}
		
		return match;
    }
}
