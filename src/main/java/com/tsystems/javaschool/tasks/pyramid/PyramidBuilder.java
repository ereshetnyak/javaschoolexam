package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
    	for (Integer elem: inputNumbers) {
            if (elem == null) {
                throw new CannotBuildPyramidException("null");
            }
        }

        int pDepth = Depth(inputNumbers.size());
        int pWidth = 2*pDepth - 1;

        int[][] pyramid = new int[pDepth][pWidth];
		Collections.sort(inputNumbers);

        int shift = 0;
        int index = inputNumbers.size() - 1;
        for (int i = pDepth - 1; i >= 0; i--) {
            int update = 0;
            int j = pWidth - 1;
            while (update < (i + 1)) {
                int elem = inputNumbers.get(index);
                pyramid[i][j - shift] = elem;
                
                j=j-2;
                update++;
                index--;
            }

            shift++;

        }
        
        return pyramid;
    }

    private static int Depth(int size) {
        int pDepth = 0;
        while (size > 0) {
            size -= pDepth;
            pDepth++;
        }
      
        if (size < 0) {
            throw new CannotBuildPyramidException("");
        } else {
            return pDepth - 1;
        }
       
    }

}
